<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class eventModel extends Model
{
    public $primaryKey = 'event_id';
    protected $table = 'event';
    public $timestamps = false;
}
