<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticketModel extends Model
{
    public $primaryKey = 'ticket_id';
    protected $table = 'ticket';
    public $timestamps = false;
}
