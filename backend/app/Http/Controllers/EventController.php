<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\eventModel;
use App\ticketModel;
use App\orderModel;

class EventController extends Controller
{
    function getEvent(){
        $eventList = eventModel::get();
        return response()->json($eventList,200);
    }

    function addEvent(Request $request){
        

        DB::beginTransaction();
        try{
            $this->validate($request,[
                'name' => 'required',
                'dateTime_event' => 'required',
                'location' => 'required'
            ]);

            $name = $request->input('name');
            $dateTime = $request->input('dateTime_event');
            $location = $request->input('location');
            
            $newEvent = new eventModel;

            $newEvent->name = $name;
            $newEvent->dateTime_event = $dateTime;
            $newEvent->location = $location;

            $newEvent->save();
            $eventUpdate = eventModel::get();

            DB::commit();
            return response()->json($eventUpdate,200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json($e->getMessage(),500);
        }
    }

    function buyTicket(Request $request){
        DB::beginTransaction();
        try{
            $id = $request->input('event_id');
            $buy = ticketModel::where('event_id','=',$id)->find(1);
            $buy->stock -= 1;

            $buy->save();
            
            DB::commit();
            return response()->json(200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(["message"=> $e->getMessage()], 500);
        }
        
    }
}
