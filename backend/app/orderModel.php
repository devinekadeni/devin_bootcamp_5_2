<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderModel extends Model
{
    public $primaryKey = 'order_id';
    protected $table = 'order';
    public $timestamps = false;
}
