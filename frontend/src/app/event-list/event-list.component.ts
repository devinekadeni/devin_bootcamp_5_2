import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  constructor(private api:ApiService) { }

  ngOnInit() {
    this.api.getEventList();
  }


  buyTicketnya(id){
    this.api.buyTicket(id);
  }
}
