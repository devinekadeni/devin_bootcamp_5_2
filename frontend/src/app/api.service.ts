import { Injectable } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService {

  constructor(private http:Http) { }

  eventList:Object[];

  getEventList(){
    this.http.get('http://localhost:8000/api/event/')
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
    .subscribe(result => this.eventList = result);
  }

  addEvent(name:string, dateTime:string, location:string){
    let data = {
      'name': name,
      'dateTime_event': dateTime,
      'location': location
    }

    let body = JSON.stringify(data);
    let headers = new Headers({
      'Content-Type':'application/json'
    });
    let options = new RequestOptions({ headers:headers });

    this.http.post('http://localhost:8000/api/event/add',body,options)
    .map(result => result.json())
    .subscribe(res => this.eventList = res);
  }

  buyTicket(id:number){
    let data = {
      'event_id': id
    }

    let body = JSON.stringify(data);
    let headers = new Headers({
      'Content-Type':'application/json'
    });
    let options = new RequestOptions({ headers:headers });

    this.http.post('http://localhost:8000/api/event/buy',body,options)
    .map(result => result.json());
  }

}
