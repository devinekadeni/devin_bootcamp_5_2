import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

  constructor(private api:ApiService) { }

  ngOnInit() {
  }

  iName:string ="";
  iDateTime:string ="";
  iLocation:string ="";

  addNewEvent(){
    this.api.addEvent(this.iName,this.iDateTime,this.iLocation);
  }
}
